README: drush_body_mangler
==========================

This is a drush script intended largely as an example of one (simple) approach
to loading and saving content nodes from a Drupal site programmatically. This
is accomplished by calling node_load() to load the content, then using basic
PHP string manipulation functions to change the content of the body field, and
then calling node_save() to store the changes back to the db.

If you need to do something like this, please test carefully and take a backup
of your site DB before doing anything live, as you can seriously mess up your
site with this tool.

See http://tranzform.ca/blog/spiderman/drush-body-mangler for more details.

<?php
# This is a hack of a script if ever ye saw one. You have been warned!

define("DEBUG", 10); # Adjust verbosity by setting this lower

/* drupal already has a function for this, right? am i really too lazy to look 
 * it up right now? here be dragons, indeed!
 **/
function strip_html($string) {
  return preg_replace('|<[^>]+>|','', $string);
}

/*
 * Another lazy helper function- should I just call dsm here or something? 
 **/
function debug($level, $string) {
  if (DEBUG >= $level) {
    echo $string;
  } 
}

# Select some nodes with a custom-rolled (and conventionally-broken) db_query() call
# This gives me a rough cut of the nodes I need. In this case, those with the token 'href'
$res = db_query("SELECT n.nid,n.title,r.body FROM {node} n LEFT JOIN {node_revisions} r ON n.vid=r.vid WHERE r.body LIKE '%s'", array('%s' => '%href%'));

# Now cycle through these nodes, looking for <a> tags that lack a title attribute
while ($row = db_fetch_array($res)) {
  $nid = $row['nid'];
  $node = node_load($nid);
  $orig_body = $node->body;
  debug(1, sprintf("title: %s(%d)\n", $node->title, $nid));
  debug(3, sprintf("body: %s\n\n", $node->body));

  # Match all link-like tags, returning an array-of-arrays of matches and their offsets
  # $matches[x][n][0] => the link in full unparsed html
  # $matches[x][n][1] => the offset of the match in the original string
  #
  # n is the number of matches (links) in the body (count($matches[0] == n)
  #
  # x varies from 0 to 2 where:
  # x=0 refers to the full link (first parenthesis in preg)
  # x=1 refers to the opening <a> tag
  # x=2 refers to the html element within the <a>
  preg_match_all("|(<a[^>]+href[^>]+>)(.+?)</a>|", $row['body'], $matches, PREG_OFFSET_CAPTURE);

  # Loop over the number of elements in 
  for ($n=0 ; $n < count($matches[0]) ; $n++) {
    debug(2,sprintf("Link %d: %s\n", $n, $matches[0][$n][0]));

    # Check for an existing title attribute
    if (preg_match('|title|', $matches[1][$n][0])) {
      debug(2,sprintf("Link %d already has a title attribute. skipping..\n", $n));
      continue;
    }

    # Otherwise insert a title attribute, stripping any html off the text 
    # first- should probably just call some core filter function here, but 
    # again: too lazy/busy to look it up right now?! #embarrassing
    $title = sprintf('title="%s" href', strip_html($matches[2][$n][0]));
    $new = preg_replace('/href/', $title, $matches[1][$n][0]);                       # new only contains the open tag, with title inserted
    debug(2,sprintf("new link %d: %s%s%s\n", $n, $new, $matches[2][$n][0], "</a>")); # in the debug output, add the element and drop anchor
    if ($new == $matches[1][$n][0]) {
      debug(1, sprintf("WARNING: Link %d did not change!\n", $n));
    }

    # escape the original open tag to produce a regex (catch special pcre chars)
    $regex = sprintf('|%s|', addcslashes($matches[1][$n][0], '|?'));
    debug(4, sprintf("regex: %s\n", $regex));

    # Finally, replace the original open tag with the new one, including title
    $node->body = preg_replace($regex, $new, $node->body);
    if ($node->body == $orig_body) {
      debug(1, sprintf("WARNING: Body did not change for link %d\n", $n));
    }
  }

  print "\n\n";
  debug(3,sprintf("new body: %s\n", $node->body));
  debug(1,"----\n");
  node_save($node);
}
